package com.vic;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties(prefix="client")
@Setter
@Getter
public class ClientProperties {
    private Long connectTimeout;
    private Long readTimeout;
    private Integer readerCount;
    private Integer writerCount;
    private List<Integer> idAccounts;
    private Integer requestCount;
    private Long threadTimeout;
    private String wsScheme;
    private String wsHost;
    private Integer wsPort;
    private String wsPath;
}
