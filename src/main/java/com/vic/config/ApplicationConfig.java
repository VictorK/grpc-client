package com.vic.config;

import com.vic.ClientProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.protobuf.ProtobufHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

@Configuration
public class ApplicationConfig {

    @Autowired
    private ClientProperties clientProperties;

    @Bean
    ProtobufHttpMessageConverter protobufHttpMessageConverter() {
        return new ProtobufHttpMessageConverter();
    }

    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplateBuilder()
                .setConnectTimeout(Duration.ofMillis(clientProperties.getConnectTimeout()))
                .setReadTimeout(Duration.ofMillis(clientProperties.getReadTimeout()))
                .build();
        restTemplate.getMessageConverters().add(protobufHttpMessageConverter());
        return restTemplate;
    }
}
