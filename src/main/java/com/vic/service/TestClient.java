package com.vic.service;

import com.vic.ClientProperties;
import com.vic.dto.BalanceProtos;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

@Service
@Slf4j
public class TestClient {

    private final RestTemplate restTemplate;
    private final ClientProperties clientProperties;
    private final ExecutorService readerThreadPool;
    private final ExecutorService writerThreadPool;

    @Autowired
    public TestClient(RestTemplate restTemplate, ClientProperties clientProperties) {
        this.restTemplate = restTemplate;
        this.clientProperties = clientProperties;
        this.readerThreadPool = Executors.newFixedThreadPool(clientProperties.getReaderCount());
        this.writerThreadPool = Executors.newFixedThreadPool(clientProperties.getWriterCount());
    }

    public void start() throws Exception {
        log.info("Start test");
        final List<CompletableFuture<Void>> tasks = new ArrayList<>();
        prepareReaderTasks(tasks);
        prepareWriterTasks(tasks);
        for (CompletableFuture<Void> task : tasks) {
            task.get(clientProperties.getThreadTimeout(), TimeUnit.MILLISECONDS);
        }
        readerThreadPool.shutdownNow();
        writerThreadPool.shutdownNow();
        log.info("Stop test");
    }

    public void start2() throws Exception {
        log.info("Start test");
        final List<CompletableFuture<Void>> tasks = new ArrayList<>();
        prepareReaderTasks2(tasks);
        prepareWriterTasks2(tasks);
        for (CompletableFuture<Void> task : tasks) {
            task.get(clientProperties.getThreadTimeout(), TimeUnit.MILLISECONDS);
        }
        readerThreadPool.shutdownNow();
        writerThreadPool.shutdownNow();
        log.info("Stop test");
    }

    private void prepareWriterTasks2(List<CompletableFuture<Void>> tasks) throws URISyntaxException {
        int countAccount = clientProperties.getIdAccounts().size();
        String url = new URI(clientProperties.getWsScheme(), null, clientProperties.getWsHost(),
                clientProperties.getWsPort(), clientProperties.getWsPath(), null, null).toString();
        for (int i = 0 ; i < clientProperties.getRequestCount(); i++) {
            Integer id = clientProperties.getIdAccounts().get(ThreadLocalRandom.current().nextInt(0, countAccount));
            tasks.add(CompletableFuture.runAsync(() ->
                    {
                        BalanceProtos.Balance balance = BalanceProtos.Balance.newBuilder()
                                .setId(id)
                                .setValue(100)
                                .build();
                        HttpEntity<String> request = new HttpEntity<>(balance.toString());
                        restTemplate.put(url, request);
                    },
                    writerThreadPool));
        }
    }

    private void prepareReaderTasks2(List<CompletableFuture<Void>> tasks) throws URISyntaxException {
        int countAccount = clientProperties.getIdAccounts().size();
        for (int i = 0 ; i < clientProperties.getRequestCount(); i++) {
            Integer id = clientProperties.getIdAccounts().get(ThreadLocalRandom.current().nextInt(0, countAccount));
            String url = new URI(clientProperties.getWsScheme(), null, clientProperties.getWsHost(),
                    clientProperties.getWsPort(), clientProperties.getWsPath() + "/" + id, null,
                    null).toString();
            tasks.add(CompletableFuture.runAsync(() -> restTemplate.getForEntity(url, BalanceProtos.Balance.class),
                    readerThreadPool));
        }
    }

    private void prepareWriterTasks(List<CompletableFuture<Void>> tasks) throws URISyntaxException {
        for (Integer id: StreamEx.of(clientProperties.getIdAccounts())) {
            String url = new URI(clientProperties.getWsScheme(), null, clientProperties.getWsHost(),
                    clientProperties.getWsPort(), clientProperties.getWsPath(), null, null).toString();
            for (int i = 0 ; i < clientProperties.getRequestCount(); i++) {
                tasks.add(CompletableFuture.runAsync(() ->
                        {
                            BalanceProtos.Balance balance = BalanceProtos.Balance.newBuilder()
                                    .setId(id)
                                    .setValue(100)
                                    .build();
                            HttpEntity<String> request = new HttpEntity<>(balance.toString());
                            restTemplate.put(url, request);
                        },
                        writerThreadPool));
            }
        }
    }

    private void prepareReaderTasks(List<CompletableFuture<Void>> tasks) throws URISyntaxException {
        for (Integer id: StreamEx.of(clientProperties.getIdAccounts())) {
            String url = new URI(clientProperties.getWsScheme(), null, clientProperties.getWsHost(),
                    clientProperties.getWsPort(), clientProperties.getWsPath() + "/" + id, null,
                    null).toString();
            for (int i = 0 ; i < clientProperties.getRequestCount(); i++) {
                tasks.add(CompletableFuture.runAsync(() -> restTemplate.getForEntity(url, BalanceProtos.Balance.class),
                        readerThreadPool));
            }
        }
    }
}
